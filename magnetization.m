function MAG0 = magnetization (x)
    [n m] = size (x);
    mag0=[];
    for a=1:n
        for b=1:m                 
            mag0=[mag0,x(a,b)];   
        endfor                       
    endfor
    MAG0=sum(mag0);
   % MAG0=MAG0/(n*m);  
endfunction