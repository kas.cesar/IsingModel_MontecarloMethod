function M = latice (n,m)
   M=[];
   for x=0:n
        N=[];
        for y=0:m
            r=round(rand(1));
            if r==0                    
                r=-1;               
            else
                r=1;
            endif
            N=[N;r];
        endfor    
    M=[M,N];
   endfor
 endfunction
 