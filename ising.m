MAGF=[];
figure;hold on;
for T = 0.1:0.5:12.1     
    M     = lattice(20,20); steps = 500;k = 1;%T = 0.1;
    %por cada 5000 steps => 6 minutos de calculo por cada experimento /lattice a 20x20
    a     = size(M);
    STEPS = []; MAG0 = []; H = []; 
    %figure;hold on;
    for pas=1:steps
        STEPS    = [STEPS;pas];
        MTest    = M; 
        d        = randi(a(1,1)*a(1,2),1,1);
        MTest(d) = MTest(d)*(-1);
        
        if hamiltonian(MTest) <= hamiltonian(M);
            M     = MTest;
            H     = [H;hamiltonian(M)];
            MAG0  = [MAG0;magnetization(M)];
        else
            if  rand(1) < exp(-(hamiltonian(MTest)-hamiltonian(M))/(k*T))
                M     = MTest;
                H     = [H;hamiltonian(M)];
                MAG0  = [MAG0;magnetization(M)];
            else
                H     = [H;hamiltonian(M)];
                MAG0  = [MAG0;magnetization(M)];
            endif
        endif
        %subplot(3,1,1);plot(STEPS,abs(H),'LineWidth',4);xlabel('steps','fontsize',18);ylabel('Hamiltonian','fontsize',18);
        %subplot(3,1,2);plot(STEPS,abs(MAG0),'linewidth',4);xlabel('steps','fontsize',18);ylabel('magnetization','fontsize',18);
        %subplot(3,1,3);imshow(M);
        %pause(0.00000000000001)
    endfor
    t=[t,T]
    MAGF=[MAGF;MAG0(length(MAG0))];
    plot(t,MAGF);xlabel('Temperature');ylabel('Final magnetization')
    pause(0.001)   
endfor
