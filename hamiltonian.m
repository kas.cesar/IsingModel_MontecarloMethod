function H0 = hamiltonian (x)
    [n m] = size (x);
    L=[]; 
    A=[]; 
    for a=1:m           
        L=[L;0];     
    endfor              
    for b=1:n+2    
        A=[A,0];   
    endfor
    M0=[L,x,L];
    M0=[A;M0;A];
    
    E0=[];
    for j=2:n+1
        E=[];
        for m=2:n+1
            e=-M0(m,j)*( M0(m-1,j) + M0(m+1,j) + M0(m,j-1) + M0(m,j+1)); 
            E=[E,e];
        endfor
        E0=[E0,E];
    endfor
    H0=sum(E0);
endfunction    